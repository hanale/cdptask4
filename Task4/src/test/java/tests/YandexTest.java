package tests;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.*;
import pages.*;
import tests.drivers.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;



public class YandexTest {

    public enum DriverCreationType {
        LocalChromeDriver,
        LocalFirefoxDriver,
        RemoteChromeDriver,
        RemoteFirefoxDriver,
    }


    private static final String START_URL = "https://mail.yandex.by";
    private static final String USER_NAME = "testCDPHanna";
    private static final String PASSWORD = "test123";
    private static final String FULL_USER_NAME = "testCDPHanna@yandex.ru";
    public static final String RECIPIENT_DRAFT_MAIL = "hanna_kurak";
    public static final String LOCAL_HOST = "http://localhost:4444/wd/hub";
    public static final String CHROME_DRIVER_PATH = "./MyWebDriver/chromedriver.exe";
    public static final String GECKO_DRIVER_PATH = "./MyWebDriver/geckodriver.exe";


    public static final String RECIPIENT = "hanna_kurak@epam.com";
    public static final String BODY = "simple test";

    private static String SUBJECT;
    private static By SUBJECT_LOCATOR;


    private static WebDriver driver;

    @BeforeClass(description = "Start browser")
    public void startBrowser() {
        DriverCreationType driverChoice = DriverCreationType.LocalFirefoxDriver;
        DriverCreator driverCreator = null;
        switch (driverChoice) {
            case LocalChromeDriver:
                driverCreator = new LocalChromeDriverCreator(CHROME_DRIVER_PATH);
                break;
            case RemoteChromeDriver:
                driverCreator = new RemoteChromeDriverCreator(LOCAL_HOST);
                break;
            case RemoteFirefoxDriver:
                driverCreator = new RemoteFireFoxDriverCreator(LOCAL_HOST);
                break;
            case LocalFirefoxDriver:
                driverCreator = new LocalFireFoxDriverCreator(GECKO_DRIVER_PATH);
                break;
            default:
                System.out.println("Choose valid driver");
        }

        try {
            driver = driverCreator.createDriver();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @BeforeClass(dependsOnMethods = "startBrowser", description = "Open YANDEX home page")
    public void openYandex() {
        driver.get(START_URL);
    }

    @BeforeClass(dependsOnMethods = "openYandex", description = "Add implicite wait and maximize window")
    public void addImplicitly() {
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        // Maximize browser window
        driver.manage().window().maximize();
    }


    @BeforeClass(dependsOnMethods = "addImplicitly", description = "Initialize subject for email")
    public void subjectInit() {
        SUBJECT = createSubject();
        System.out.println("Subject:   " + SUBJECT);
        SUBJECT_LOCATOR = By.xpath(createSubjectLocator());
    }


    @Test(description = "Login to YANDEX")
    public void loginToYandex() {
        boolean loginIsComplete = new YandexSignInPage(driver)
                .loginToYandex(USER_NAME, PASSWORD)
                .isDisplayed(FULL_USER_NAME);
        Assert.assertTrue(loginIsComplete, "Looks you are NOT logged in correctly");
    }

    @Test(dependsOnMethods = "loginToYandex", description = "Create draft email")
    public void createDraftMail() {
        boolean draftIsCreated = new YandexInboxPage(driver).navigateToDraftEmailPage().
                createNewMail(RECIPIENT, SUBJECT, BODY).isEmailDisplayed(SUBJECT);
        Assert.assertTrue(draftIsCreated, "Email is NOT created");
    }


    @Test(dependsOnMethods = "createDraftMail", description = "Verify data in draft email")
    public void verifyDraftEmailData() {
        boolean recepientIsCorrect = new YandexEmailListPage(driver).navigateToDraftEmail(SUBJECT_LOCATOR).
                draftMailRecipientVerification(RECIPIENT_DRAFT_MAIL);
        Assert.assertTrue(recepientIsCorrect, "Recepient is NOT valid");

        boolean subjectIsCorrect = new YandexDraftEmailPage(driver).draftMailSubjectVerification(SUBJECT);
        Assert.assertTrue(subjectIsCorrect, "Subject is NOT valid");

        boolean bodyIsCorrect = new YandexDraftEmailPage(driver).draftMailBodyVerification(BODY);
        Assert.assertTrue(bodyIsCorrect, "Body is NOT valid");
    }


    @Test(dependsOnMethods = "verifyDraftEmailData", description = "Sent email")
    public void sentEmail() {
        boolean isEmailSent = new YandexDraftEmailPage(driver).sentEmail().isEmailSent();
        Assert.assertTrue(isEmailSent, "Email is NOT sent");
    }


    @Test(dependsOnMethods = "sentEmail", description = "Verify email is displayed in Sent email folder")
    public void verifySentFolder() {
        boolean sentEmailExist = new YandexInboxPage(driver).navigateToSentFolder().isEmailDisplayed(SUBJECT);
        Assert.assertTrue(sentEmailExist, "Email with the subject doesn't exist in the folder");
    }

    @Test(dependsOnMethods = "verifySentFolder", description = "Verify that email is deleted from sent folder")
    public void deleteSentEmailviaContextMenu() {
        if (driver.getClass().toString().contains("Chrome")) {
            new YandexEmailListPage(driver).deleteEmailViaContextMenu(SUBJECT_LOCATOR);
            boolean sentEmailExist = new YandexInboxPage(driver).navigateToSentFolder().isEmailDisplayed(SUBJECT);
            Assert.assertTrue(!sentEmailExist, "Email with the subject exists in the folder");
        } else if (driver.getClass().getName().contains("firefox")) {
            System.out.println("deleteSentEmailviaContextMenu test is skipped for FireFox");
        }
    }

    @Test(dependsOnMethods = "deleteSentEmailviaContextMenu", description = "Verify email is NOT displayed in DRAFT folder")
    public void verifyDraftFolder() {
        boolean draftEmailExist = new YandexEmailListPage(driver).navigateToDraftFolder().
                isEmailDisplayed(SUBJECT);
        Assert.assertTrue(!draftEmailExist, "Email is displayed");
    }


    @Test(dependsOnMethods = "verifyDraftFolder", description = "Logout from YANDEX")
    public void logOff() {
        boolean isLoggOff = new YandexEmailListPage(driver).clickOnLogOff().isLoggOff();
        Assert.assertTrue(isLoggOff, "Looks you are NOT logged off correctly");
    }


    @AfterClass(description = "Stop Browser")
    public void stopBrowser() {
        driver.close();
        driver = null;
    }


    public String createSubject() {
        return RandomStringUtils.randomAlphabetic(10);
    }

    public String createSubjectLocator() {
        String subjectLocator = "(//span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_subject']/span[@title='";
        subjectLocator = subjectLocator + SUBJECT + "'])[1]";
        return subjectLocator;
    }

}