package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class YandexEmailListPage extends YandexPage {


    @FindBys(@FindBy(xpath = "//span[@class ='mail-MessageSnippet-Item mail-MessageSnippet-Item_subject']/span"))
    private List<WebElement> emails;


    @FindBy(xpath = "//a[@href='#draft']")
    private WebElement draftFolderLink;

    @FindBy(xpath = "//div[@class='mail-User-Name']")
    private WebElement headerUserName;

    @FindBy(xpath = "//div[@class='mail-User-Name']")
    private WebElement exitButton;


    @FindBy(xpath = "//span[@class='context-menu-item__icon context-menu-item__icon_delete']")
    private WebElement deleteIcon;

    public YandexEmailListPage(WebDriver driver) {
        super(driver);
    }

    public YandexDraftEmailPage navigateToDraftEmail(By subjectLocator) {

        driver.findElement(subjectLocator).click();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Draft folder is opened");
        return new YandexDraftEmailPage(driver);
    }

    public boolean isEmailDisplayed(String subject) {
        return isEmailWithSubjectExistInList(emails, subject);
    }

    private boolean isEmailWithSubjectExistInList(final List<WebElement> emailsList, final String subject) {
        boolean isEmailExist = false;
        for (WebElement email : emailsList) {
            if (email.getText().equals(subject)) {
                isEmailExist = true;
            }
        }
        return isEmailExist;
    }

    public YandexEmailListPage navigateToDraftFolder() {
        draftFolderLink.click();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Draft folder is opened");
        return new YandexEmailListPage(driver);
    }

    public YandexSignInPage clickOnLogOff() {
        highlightElement(headerUserName);
        headerUserName.click();
        exitButton.click();
        System.out.println("You are logged off");
        return new YandexSignInPage(driver);
    }


    public YandexEmailListPage deleteEmailViaContextMenu(By subjectLocator) {
        new Actions(driver).contextClick(driver.findElement(subjectLocator)).build().perform();
        deleteIcon.click();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return new YandexEmailListPage(driver);
    }

}
